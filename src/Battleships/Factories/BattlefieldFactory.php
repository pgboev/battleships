<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:37 PM
 */

namespace Battleships\Factories;

use Battleships\Models\Battlefield;
use Battleships\Models\Battleship;
use Battleships\Models\Destroyer;

class BattlefieldFactory
{
    public function createBattlefield()
    {
        $battlefield = new Battlefield();
        $battlefield->addShip(new Battleship());
        $battlefield->addShip(new Destroyer());
        $battlefield->addShip(new Destroyer());

        return $battlefield;
    }
}