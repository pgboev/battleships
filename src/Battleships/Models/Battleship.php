<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:02 PM
 */

namespace Battleships\Models;

class Battleship extends Ship
{
    protected $size = 5;
    protected $armor = 5;
}