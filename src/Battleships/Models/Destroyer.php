<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:01 PM
 */

namespace Battleships\Models;

class Destroyer extends Ship
{
    protected $size = 4;
    protected $armor = 4;
}