<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:02 PM
 */

namespace Battleships\Models;

use Battleships\Interfaces\ShipInterface;

abstract class Ship implements ShipInterface
{
    protected $size;
    protected $armor;
    private $startPosition;
    private $endPosition;

    public function getSize()
    {
        return $this->size;
    }

    public function getArmor()
    {
        return $this->armor;
    }

    public function getStartPosition()
    {
        return $this->startPosition;
    }

    public function setStartPosition($value)
    {
        $this->startPosition = $value;
    }

    public function getEndPosition()
    {
        return $this->endPosition;
    }

    public function setEndPosition($value)
    {
        $this->endPosition = $value;
    }
}