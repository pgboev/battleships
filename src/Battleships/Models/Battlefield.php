<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:01 PM
 */

namespace Battleships\Models;

use Battleships\Interfaces\BattlefieldInterface;
use Battleships\Interfaces\ShipInterface;

/**
 * Class Battlefield
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Battlefield implements BattlefieldInterface
{
    protected $size = 10;
    protected $ships = [];
    private $showShips = false;

    public function __construct()
    {
        //
    }

    public function addShip(ShipInterface $ship)
    {
        list($startPosition, $endPosition) = $this->randomPositionGenerator($ship->getSize());

        $ship->setStartPosition($startPosition);
        $ship->setEndPosition($endPosition);

        $this->ships[] = $ship;
    }

    public function showShips()
    {
        $this->showShips = true;
    }

    public function __toString()
    {
        $colsPositionMarks = [];
        $rowsPositionMarks = [];

        for ($i = 1; $i <= $this->size; $i++) {

            $colsPositionMarks[] = $i;
            $rowsPositionMarks[$i] = chr(64 + $i);
        }

        $battlefieldArray = array();
        $battlefieldArray[] = '   ' . implode("  ", $colsPositionMarks) . PHP_EOL;

        for ($row = 1; $row <= $this->size; $row++) {

            If ($this->showShips) {

                $rowString = $rowsPositionMarks[$row];

                for ($col = 1; $col <= $this->size; $col++) {

                    $shipOnPosition = $this->checkForShipOnPosition($row, $col);

                    if($shipOnPosition){

                        $rowString .= '  X';
                    } else {

                        $rowString .= '  .';
                    }
                }

                $rowString .= PHP_EOL;

                $battlefieldArray[] = $rowString;
            } else {

                $battlefieldArray[] = $rowsPositionMarks[$row] . str_repeat("  .", 10) . PHP_EOL;
            }
        }

        $battlefieldString = implode("", $battlefieldArray);

        return $battlefieldString;
    }

    /**
     * @param $row
     * @param $col
     * @return bool
     */
    public function checkForShipOnPosition($row, $col): bool
    {
        //TODO: Move this to utils
        $shipOnPosition = false;

        for ($i = 0; $i < count($this->ships); $i++) {

            $ship = $this->ships[$i];

            $shopStartPosition = $ship->getStartPosition();
            $shipStartPositionRow = str_split($shopStartPosition)[0];
            $shipStartPositionRow = ord($shipStartPositionRow) - 64;
            $shipStartPositionCol = substr($shopStartPosition, 1);
            $shipStartPositionCol = intval($shipStartPositionCol);

            $shipEndPosition = $ship->getEndPosition();
            $shipEndPositionRow = str_split($shipEndPosition)[0];
            $shipEndPositionRow = ord($shipEndPositionRow) - 64;
            $shipEndPositionCol = substr($shipEndPosition, 1);
            $shipEndPositionCol = intval($shipEndPositionCol);

            if ($shipStartPositionRow === $row && ($col >= $shipStartPositionCol && $col <= $shipEndPositionCol)
                || $shipStartPositionCol === $col && ($row >= $shipStartPositionRow && $row <= $shipEndPositionRow)) {

                $shipOnPosition = true;
            }
        }

        return $shipOnPosition;
    }

    /**
     * @param $startPosition
     * @param $endPosition
     * @return bool
     */
    public function checkForShipBetweenPositions($startPosition, $endPosition): bool
    {
        //TODO: Move this to utils
        $shipBetweenPositions = false;

        $givenStartPosition = $startPosition;
        $givenStartPositionRow = str_split($givenStartPosition)[0];
        $givenStartPositionRow = ord($givenStartPositionRow) - 64;
        $givenStartPositionCol = substr($givenStartPosition, 1);
        $givenStartPositionCol = intval($givenStartPositionCol);

        $givenEndPosition = $endPosition;
        $givenEndPositionRow = str_split($givenEndPosition)[0];
        $givenEndPositionRow = ord($givenEndPositionRow) - 64;
        $givenEndPositionCol = substr($givenEndPosition, 1);
        $givenEndPositionCol = intval($givenEndPositionCol);

        //TODO: Calculate lines intersection

        return $shipBetweenPositions;
    }

    /**
     * @param $shipSize
     * @return array
     */
    public function randomPositionGenerator($shipSize): array
    {
        //TODO: Move this to utils
        $randomRowNumber = rand(1, $this->size);
        $randomColNumber = rand(1, $this->size);

        $battlefieldSize = $this->size;
        $shipSize = $shipSize - 1;

        $startPosition = chr(64 + $randomRowNumber) . $randomColNumber;
        $endPosition = null;

        //Here we are positioning the ships always from left to right or top to bottom
        if (($randomColNumber + $shipSize) <= $battlefieldSize) {

            $endPosition = chr(64 + $randomRowNumber) . ($randomColNumber + $shipSize);
        } else if (($randomRowNumber + $shipSize) <= $battlefieldSize) {

            $endPosition = chr((64 + $randomRowNumber) + $shipSize) . $randomColNumber;
        } else if (($randomColNumber - $shipSize) >= 1) {

            $startPosition = chr(64 + $randomRowNumber) . ($randomColNumber - $shipSize);
            $endPosition = chr(64 + $randomRowNumber) . $randomColNumber;
        } else if (($randomRowNumber - $shipSize) >= 1) {

            $startPosition = chr((64 + $randomRowNumber) - $shipSize) . $randomColNumber;
            $endPosition = chr(64 + $randomRowNumber) . $randomColNumber;
        }

        $generatedPositions = array($startPosition, $endPosition);

        //TODO: Check for lines intersection

        return $generatedPositions;
    }
}
