<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:47 PM
 */

namespace Battleships\Interfaces;

interface ShipInterface
{
    public function getSize();
    public function getArmor();
    public function getStartPosition();
    public function setStartPosition($value);
    public function getEndPosition();
    public function setEndPosition($value);
}