<?php
/**
 * Created by PhpStorm.
 * User: boev
 * Date: 5/10/2018
 * Time: 11:47 PM
 */

namespace Battleships\Interfaces;

interface BattlefieldInterface
{
    public function addShip(ShipInterface $ship);
    public function showShips();
}