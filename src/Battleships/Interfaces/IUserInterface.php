<?php
/**
 * Created by PhpStorm.
 * User: Boevsson
 * Date: 12/10/2018
 * Time: 1:09 PM
 */

namespace Battleships\Interfaces;

interface IUserInterface
{
    public function read();
    public function write();
}