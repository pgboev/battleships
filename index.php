<?php

namespace Battleships;

use Battleships\Factories\BattlefieldFactory;

include 'bootstrap/bootstrap.php';

session_start();

$battlefieldFactory = new BattlefieldFactory();

$battlefield = $battlefieldFactory->createBattlefield();

//TODO: This should dependant on the UI
if($_POST['command'] === 'show'){

    $battlefield->showShips();
}

//TODO: Shooting functionality

print '<pre>' . $battlefield . '</pre>';

?>

<form name="input" action="index.php" method="post">
    Enter coordinates (row, col), e.g. A5 <input type="input" size="5" name="command" autocomplete="off" autofocus="">
    <input type="submit">
</form>